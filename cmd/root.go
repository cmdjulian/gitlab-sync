package cmd

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/go-git/go-git/v5"
	"github.com/itchyny/gojq"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"sort"
	"strings"

	gitHttp "github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "gitlab-sync",
	Short: "Sync and update repositories from a GitLab group",
	Long: `This CLI tool helps you synchronize GitLab Groups to your local filesystem.
All sub groups and repositories are synced to you locale filesystem while keeping the overall structure.`,
	Run: run,
}

type Protocol string

const (
	SSH   Protocol = "ssh"
	HTTPS Protocol = "https"
	HTTP  Protocol = "http"
)

type ProjectType string

const (
	USER  ProjectType = "user"
	GROUP ProjectType = "group"
)

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.PersistentFlags().StringP("token", "t", "", "GitLab token with api scope")
	rootCmd.PersistentFlags().StringP("protocol", "p", "ssh", "selected Git protocol, has to be 'ssh', 'https' or 'http'")
	rootCmd.PersistentFlags().StringP("domain", "d", "gitlab.com", "domain of the GitLab instance")
	rootCmd.PersistentFlags().StringP("group", "g", "", "GitLab group to sync (default is the current folder name if nothing is supplied)")
	rootCmd.PersistentFlags().StringP("folder", "f", "", "local folder to sync against (default is $PWD)")
	rootCmd.PersistentFlags().BoolP("use-http", "", false, "use http for communication with the GitLab API. This should only be used for development")
	rootCmd.PersistentFlags().BoolP("insecure-skip-tls-verify", "", false, "skip tls verify for api server config and 'git clone' command. Should only be used for development")
	rootCmd.PersistentFlags().StringP("kind", "k", "group", "select if the supplied group is a GitLab group or a user account namespace. For group select 'group', for user 'user'.")
	rootCmd.PersistentFlags().BoolP("preview", "", false, "don't clone anything just preview to found projects")
	rootCmd.PersistentFlags().BoolP("print", "", false, "show all flags with their corresponding value after default resolution")

	if rootCmd.MarkPersistentFlagRequired("token") != nil {
		return
	}
}

func pwd() string {
	pwd, err := os.Getwd()
	if err != nil {
		panic("couldn't get pwd")
	}

	return pwd
}

func printAsTable(rows []table.Row) {
	t := table.NewWriter()
	t.SetOutputMirror(os.Stdout)
	t.AppendHeader(table.Row{"project", "local path", "operation"})
	t.AppendRows(rows)
	t.Render()
}

func run(cmd *cobra.Command, _ []string) {
	token, _ := cmd.Flags().GetString("token")
	p, _ := cmd.Flags().GetString("protocol")
	protocol := toProtocol(p)
	domain, _ := cmd.Flags().GetString("domain")
	group, _ := cmd.Flags().GetString("group")
	folder, _ := cmd.Flags().GetString("folder")
	useHttp, _ := cmd.Flags().GetBool("use-http")
	insecureSkipTlsVerify, _ := cmd.Flags().GetBool("insecure-skip-tls-verify")
	k, _ := cmd.Flags().GetString("kind")
	kind := toProjectType(k)
	preview, _ := cmd.Flags().GetBool("preview")
	shouldPrint, _ := cmd.Flags().GetBool("print")

	if folder == "" {
		folder = pwd()
	}

	if group == "" {
		group = path.Base(folder)
	}

	if shouldPrint {
		log.Printf("token: %s", token)
		log.Printf("protocol: %s", protocol)
		log.Printf("domain: %s", domain)
		log.Printf("group: %s", group)
		log.Printf("folder: %s", folder)
		log.Printf("use-http: %t", useHttp)
		log.Printf("insecure-skip-tls-verify: %t", insecureSkipTlsVerify)
		log.Printf("kind: %s", kind)
		log.Printf("preview: %t", preview)
		println()
	}

	projectsJson := downloadProjectJson(domain, group, token, useHttp, insecureSkipTlsVerify, kind)
	projects := jq(projectsJson, domain, group)

	if len(projects) <= 0 {
		log.Println("no projects found")
		return
	}

	sort.Strings(projects)

	if preview {
		previewChanges(folder, projects)
		return
	}

	for _, project := range projects {
		cloneOrPull(folder, protocol, domain, group, project, token, insecureSkipTlsVerify)
	}
}

func previewChanges(folder string, projects []string) {
	var rows []table.Row

	for _, project := range projects {
		var pull bool
		localFolder := filepath.Join(folder, fileNameWithoutExtTrimSuffix(project))
		_, err := os.Stat(localFolder)
		if err == nil {
			pull = true
		} else {
			pull = false
		}

		if pull {
			rows = append(rows, table.Row{project, localFolder, "pull"})
		} else {
			rows = append(rows, table.Row{project, localFolder, "clone"})
		}
	}

	printAsTable(rows)
}

func toProtocol(protocol string) Protocol {
	switch protocol {
	case string(SSH):
		return SSH
	case string(HTTPS):
		return HTTPS
	case string(HTTP):
		return HTTP
	default:
		log.Fatalf("supplied protocol '%s' is not a known protocol. Known protocols are 'ssh', 'https' and 'http'", protocol)
	}

	// unreachable code
	return SSH
}

func toProjectType(projectType string) ProjectType {
	switch projectType {
	case string(USER):
		return USER
	case string(GROUP):
		return GROUP
	default:
		log.Fatalf("supplied kind '%s' is not a known kind. Known kinds are 'user' and 'group'", projectType)
	}

	// unreachable code
	return GROUP
}

func cloneOrPull(path string, protocol Protocol, domain string, group string, project string, token string, insecureSkipTlsVerify bool) {
	localFolder := filepath.Join(path, fileNameWithoutExtTrimSuffix(project))

	shouldPull := ensureDir(localFolder)
	if shouldPull {
		log.Printf("pulling repo '%s/%s' in '%s'", group, project, localFolder)
		// We instantiate a new repository targeting the given path (the .git folder)
		r, err := git.PlainOpen(localFolder)
		if err != nil {
			if err.Error() == "repository does not exist" {
				log.Println("git url doesn't seem to contain a valid git repository, maybe the repository is empty?")
				println()
				return
			}

			log.Printf("could not open repo: %s", err)
			println()
			return
		}

		// Get the working directory for the repository
		w, err := r.Worktree()
		if err != nil {
			log.Printf("could not open worktree: %s", err)
			return
		}

		// Pull the latest changes from the origin remote and merge into the current branch
		err = w.Pull(&git.PullOptions{RemoteName: "origin"})
		if err != nil {
			if err.Error() == "already up-to-date" {
				log.Printf("already up-to-date")
				println()
				return
			} else {
				log.Printf("could not pull repo: %s", err)
				return
			}
		}

		// Print the latest commit that was just pulled
		ref, _ := r.Head()
		commit, _ := r.CommitObject(ref.Hash())

		fmt.Println(commit)
		println()
	} else {
		log.Printf("cloning repo '%s/%s' to '%s'", group, project, localFolder)
		var err error
		switch protocol {
		case SSH:
			_, err = git.PlainClone(localFolder, false, &git.CloneOptions{
				URL:      sshCloneUrl(domain, group, project),
				Progress: os.Stdout,
			})
		case HTTPS:
			var auth gitHttp.AuthMethod
			auth = &gitHttp.BasicAuth{Username: "PRIVATE-TOKEN", Password: token}
			_, err = git.PlainClone(localFolder, false, &git.CloneOptions{
				Auth:            auth,
				URL:             httpsCloneUrl(domain, group, project),
				Progress:        os.Stdout,
				InsecureSkipTLS: insecureSkipTlsVerify,
			})
		case HTTP:
			var auth gitHttp.AuthMethod
			auth = &gitHttp.BasicAuth{Username: "PRIVATE-TOKEN", Password: token}
			_, err = git.PlainClone(localFolder, false, &git.CloneOptions{
				Auth:     auth,
				URL:      httpCloneUrl(domain, group, project),
				Progress: os.Stdout,
			})
		}

		if err != nil {
			message := err.Error()
			sshKeyError := "ssh: handshake failed: ssh: unable to authenticate, attempted methods [none publickey], no supported methods remain"
			if message == sshKeyError {
				log.Fatalf("could not clone from repository because ssh handshake failed. Is you key correctlly configured, unlocked and setup for gitlab?")
			}
			log.Printf("could not clone repo: %s", err)
			return
		}
		println()
	}
}

func fileNameWithoutExtTrimSuffix(fileName string) string {
	return strings.TrimSuffix(fileName, filepath.Ext(fileName))
}

func sshCloneUrl(domain string, group string, project string) string {
	return fmt.Sprintf("git@%s:%s/%s", domain, group, project)
}

func httpsCloneUrl(domain string, group string, project string) string {
	return fmt.Sprintf("https://%s/%s/%s", domain, group, project)
}

func httpCloneUrl(domain string, group string, project string) string {
	return fmt.Sprintf("http://%s/%s/%s", domain, group, project)
}

func downloadProjectJson(domain string, group string, token string, useHttp bool, insecureSkipTlsVerify bool, kind ProjectType) []interface{} {
	var protocol string
	if useHttp {
		protocol = "http"
	} else {
		protocol = "https"
	}

	var client http.Client
	if insecureSkipTlsVerify {
		customTransport := http.DefaultTransport.(*http.Transport).Clone()
		customTransport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
		client = http.Client{Transport: customTransport}
	} else {
		client = http.Client{}
	}

	var typeString string
	switch kind {
	case GROUP:
		typeString = "groups"
	case USER:
		typeString = "users"
	}

	url := fmt.Sprintf("%s://%s/api/v4/%s/%s/projects?include_subgroups=true&per_page=1000", protocol, domain, typeString, group)
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("PRIVATE-TOKEN", token)
	resp, err := client.Do(req)
	if err != nil {
		panic("could not retrieve GitLab Project list")
	}

	if resp.StatusCode == 401 {
		log.Fatalf("Token seems to be wrong")
	}

	if resp.StatusCode == 404 {
		log.Fatalf("Group does not exist. If this is a project located under a personal user profile, try to re-run the programm with the '-k user' flag.")
	}

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		panic("could not read response")
	}

	var result []interface{}
	if json.Unmarshal(b, &result) != nil {
		panic("could not deserialize json response from GitLab")
	}

	return result
}

func jq(projectsJson []interface{}, domain string, group string) []string {
	query, err := gojq.Parse(".[].http_url_to_repo")
	if err != nil {
		log.Fatalln(err)
	}
	iter := query.Run(projectsJson)
	prefix := fmt.Sprintf("\"https://%s/%s/", domain, group)
	var projects []string
	for {
		v, ok := iter.Next()
		if !ok {
			break
		}
		if err, ok := v.(error); ok {
			log.Fatalln(err)
		}

		project := fmt.Sprintf("%#v", v)
		project = project[len(prefix) : len(project)-1]
		projects = append(projects, project)
	}

	return projects
}

func ensureDir(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}

	if err := os.MkdirAll(path, os.ModePerm); os.IsExist(err) {
		return true
	}

	return false
}
