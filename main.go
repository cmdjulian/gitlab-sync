package main

import (
	"gitlab.com/cmdjulian/gitlab-sync/cmd"
	"log"
)

func main() {
	log.SetFlags(log.Flags() &^ (log.Ldate | log.Ltime))
	cmd.Execute()
}
